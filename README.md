# Description

Some configuration files for personal use.

[![Hits-of-Code](https://hitsofcode.com/gitlab/nicoty/dotfiles)](https://hitsofcode.com/view/gitlab/nicoty/dotfiles)

## License

This Work is distributed and dual-licensed under the terms of both the [MIT License](LICENSE-MIT) and the [Apache License 2.0](LICENSE-APACHE).

### Contribution

Unless You explicitly state otherwise, any Contribution submitted for inclusion in the Work by You, as defined in the Apache License 2.0, shall be dual-licensed as above, without any additional terms or conditions.
