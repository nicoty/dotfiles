#!/bin/sh

# from rustup, delete this and the following line after uninstall
export PATH="$HOME/.cargo/bin:$PATH"

# for nodejs, delete after uninstalling nodejs packages
export PATH="$HOME/node_modules/.bin:$PATH"

# added #

. "$HOME/scripts/freepass.sh"

export LESSHISTFILE="-"
export XKB_DEFAULT_LAYOUT="gb"
export PATH="$HOME/.local/bin:$PATH"
export MANPATH="$HOME/.local/share/man:"
export EDITOR="/bin/micro"
export PAGER="/bin/less"
export CC="/bin/clang" # used by cargo
export CXX="/bin/clang++" # used by cargo
#export CC="/bin/gcc" # used when installing indradb
#export CXX="/bin/g++" # used when installing indradb
export BAT_PAGER="/bin/less" # used by bat
export QT_QPA_PLATFORMTHEME="qt5ct"

alias sudo="sudo "
alias s="sudo "
alias reboot="sudo reboot "
alias restart="sudo reboot "
alias poweroff="sudo poweroff "
alias shutdown="sudo shutdown "
alias sn="sudo shutdown -P now"
alias suspend="sudo zzz"
alias hibernate="sudo ZZZ"
alias zzz="sudo zzz"
alias ZZZ="sudo ZZZ"

#alias ls="ls -l --full-time --color=auto "
#alias l="ls -la --full-time --color=auto "
#alias ld="ls -d .* --full-time --color=auto "
#alias ll="ls -l --full-time --color=auto "
#alias la="ls -la --full-time --color=auto "

alias l="exa -aglm --color=auto "
alias la="exa -aglm --color=auto "
alias lc="exa -al --color=always "
alias lac="exa -alm --color=always "
alias ld="exa -d .* -m --color=auto "
alias ldc="exa -d .* -m --color=always "

# prevent clobbering
alias mv="mv -i "
alias cp="cp -i "

alias grep="rg "
#alias find="fd "
alias c="bat "
alias sc="sudo bat "
alias e="micro "
#alias cloc="tokei "
#alias sed="sd "
alias upgrade="$HOME/bin/update "
alias sinstall="sudo xbps-install -Sv "
alias suninstall="sudo xbps-remove -Rv "
alias sremove="sudo xbps-remove -Rv "
alias profile="micro $HOME/.profile"
alias path="echo $PATH"
alias notes="micro $HOME/documents/void.txt"
alias history="micro $HOME/.local/share/ion/history"
alias gclone="git clone --recursive "
alias gpull="git pull --recurse-submodules"
alias gpush="git push "
alias bc="bc -l "
alias scroff="xset dpms force off"

# games
alias awkaster.awk="gawk -f /home/a/development/github/TheMozg/awk-raycaster/awkaster.awk"
alias snake.sed="/home/a/development/github/jinchizhong/sed-snake/keyseq.sh | sed -f /home/a/development/github/jinchizhong/sed-snake/snake.sed"
alias snake.sh="bash /home/a/development/github/bittorf/shell_snake/snake.sh"
alias arkanoid.sed="bash /home/a/development/github/aureliojargas/SedArkanoid/playsed /home/a/development/github/aureliojargas/SedArkanoid/arkanoid.sed"
alias sedtris.sed="bash /home/a/development/github/aureliojargas/SedArkanoid/playsed /home/a/development/github/uuner/sedtris/sedtris.sed"
alias gravity-defied.sed="cd /home/a/development/github/Firemoon777/gravity-defied/ && ./play.sh"
alias dustship.sh="sh /home/a/development/github/j8r/DustShip/dustship.sh"
alias awktc.awk="/home/a/development/github/mikkun/AWKTC/awktc.awk"
alias piu-piu.sh="/home/a/development/github/vaniacer/piu-piu-SH/piu-piu"
alias arkanoid.sh="/home/a/development/github/Andrew8xx8/shell-games/arkanoid.sh"
alias shellshock.sh="/home/a/development/github/b01t/shellshock/shellshock"
alias git-adventure-game.sh="/home/a/development/github/bloomberg/git-adventure-game/start-game"
alias mari0="love /home/a/development/github/Stabyourself/mari0.love"
alias curseofwar="/home/a/development/github/rofl0r/curseofwar/curseofwar"
cd "$HOME"
