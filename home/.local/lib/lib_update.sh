#!/bin/sh

# dependencies: `lib.sh`, `curl`, `sudo`, `git`

# include libs
  sh_lib="${HOME}/.local/lib/lib.sh"

# shellcheck source=./lib.sh
  . "${sh_lib}" &&

error_configuration_invalid () { (
  error "Invalid line ${number_line:?} in ${conf_self:?}:\n${configuration:?}\n" "2" >&2
) }

# accepts url as 1st argument, downloads it to a file using 2nd argument as the filename. subshell is used to keep variables used in the local scope of the function.
sudo_download_file () { (
  file_temporary="$(mktemp)"

  rm_file_temporary () {
    rm "${file_temporary}"
  }

  trap rm_file_temporary EXIT HUP INT QUIT ABRT ALRM TERM &&

  curl "${1}" --output "${file_temporary}" &&
# `install` is not specified in POSIX.
  sudo install --compare "${file_temporary}" "${2}"
) }

# return 1 if already up to date, else git clone or pull from master if local repository exists already and return 0. WARNING: REMOVES CHANGES TO LOCAL REPO!
git_repository_update () { (
  directory_parent="${1}"
  repository_local="${2}"
  repository_remote="${3}"
  branch="${4}"

# check if parent directory exists
  if test -d "$(readlink --canonicalize "${directory_parent}")" ; then
#   check if local repository exists
    if test -d "$(readlink --canonicalize "${repository_local}/.git")" ; then
#     it does, so check if up to date
      git -C "${repository_local}" remote update &&
      if test "$(git -C "${repository_local}" rev-parse HEAD)" = "$(git -C "${repository_local}" rev-parse origin/"${branch}")" ; then
#       already up to date, return 1
        return 1
      else
#       it's not, so update then do stuff
        git -C "${repository_local}" reset --hard HEAD &&
        git -C "${repository_local}" fetch --all --recurse-submodules &&
        git -C "${repository_local}" reset --hard origin/"${branch}"
      fi
#   it doesn't, so clone it then do stuff
    else
      git -C "${directory_parent}" clone --recursive --branch "${branch}" --single-branch "${repository_remote}"
    fi
# it doesn't, so make it then clone repo then do stuff
  else
    mkdir --parents "${directory_parent}" &&
    git -C "${directory_parent}" clone --recursive --branch "${branch}" --single-branch "${repository_remote}"
  fi
) }
