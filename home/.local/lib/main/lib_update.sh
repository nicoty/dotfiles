#!/bin/sh

main () { (
# -e = exit if any statement returns a non-true return value. -u = exit if an attempt is made to use an uninitialised variable.
  set -eu &&

# include libs
# directory that contains this script
  directory_self="$(dirname "$(readlink -f "${0}")")"
  file_self="$(basename "$(readlink -f "${0}")")"
  sh_lib="${directory_self}/../lib.sh"

# shellcheck source=../lib.sh
  . "${sh_lib}" &&

# set trap
  trap 'notify_exit_early ""' HUP INT QUIT ABRT ALRM TERM &&

# warn
  warning_privilege &&

# set variables
  version_self="0.1.0"
# will_exit used to allow other options to be parsed too
  will_exit=1

# define internal functions
  print_help () {
    printf 'Some functions used by update scripts.

USAGE:
  To be able to use the functions this library
  script provides, insert the following line into
  your script:

  . %s

FUNCTIONS:
  error_configuration_invalid
    Used when applying configuration for
    executable scripts. Print "Invalid line NUMBER
    in CONFIGURATION:\\nLINE" in red, where
    CONFIGURATION is the configuration file used,
    LINE is invalid line the file and NUMBER is
    the relevant line number.

  sudo_download_file <URL> <OUTPUT>
    Download a file from URL to a temporary file
    with a randomised name in /tmp/. As "root",
    Install the downloaded file to OUTPUT,
    otherwise remove it if the download was
    unsuccessful.

  git_repository_update <PARENT> <LOCAL> <REMOTE>
  <BRANCH>
    Make directory PARENT if needed, then pull
    branch BRANCH of repository REMOTE into it. If
    a local repository (LOCAL) already exists,
    update it instead. If it is already
    up-to-date, return 1 instead. This function
    removes any local edits to LOCAL.
' "$(readlink --canonicalize "${directory_self}/../${file_self}")" >&2
  }

# argument parsing loop from https://stackoverflow.com/a/28466267
  while getopts hVx-: argument; do
    case "${argument}" in
      h )
        print_help >&2 &&
        will_exit=0
      ;;
      V )
        print_version "${file_self}" "${version_self}" >&2 &&
        will_exit=0
      ;;
      x )
        set -x
      ;;
      - )
        case "${OPTARG}" in
          help )
            print_help >&2 &&
            will_exit=0
          ;;
          version )
            print_version "${file_self}" "${version_self}" >&2 &&
            will_exit=0
          ;;
          debug )
            set -x
          ;;
          help* | version* | debug* )
            error_arguments_contained >&2 &&
            exit 2
          ;;
#         "--" terminates argument processing
          '' )
            break
          ;;
          * )
            error_option_invalid >&2 &&
            exit 2
          ;;
        esac
      ;;
#     getopts already reported the illegal option
      \? )
        exit 2
      ;;
    esac
  done &&
# remove parsed options and arguments from "${@}" list
  shift $((OPTIND-1)) &&

  if test "${will_exit}" -eq 0 ; then
    exit 0
  fi
) }

main "${@}"
