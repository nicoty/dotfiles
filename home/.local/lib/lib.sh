#!/bin/sh

# -e = exit if any statement returns a non-true return value. -u = exit if an attempt is made to use an uninitialised variable.
set -eu &&

# prints version of script
print_version () { (
  name_file="${1}"
  version="${2}"

  printf '%s %s
' "${name_file}" "${version}" >&2
) }

# prints coloured text
print_coloured () { (
  text="${1}"
  colour="$(printf '%s' "${2}" | tr '[:upper:]' '[:lower:]')"

  case "${colour}" in
    "30" | "black" )
    code_colour="30"
    ;;
    "31" | "r" | "red" )
    code_colour="31"
    ;;
    "32" | "g" | "green" )
    code_colour="32"
    ;;
    "33" | "y" | "yellow" )
    code_colour="33"
    ;;
    "34" | "b" | "blue" )
    code_colour="34"
    ;;
    "35" | "m" | "magenta" )
    code_colour="35"
    ;;
    "36" | "c" | "cyan" )
    code_colour="36"
    ;;
    "37" | "w" | "white" )
    code_colour="37"
    ;;
    * )
      printf '\033[31mERROR: "%s" is not a valid colour.\033[0m' "${colour}" &&
      exit 2
    ;;
  esac &&
  printf '\033[%sm%b\033[0m' ${code_colour} "${text}"
) }

error () { (
  text="${1}"
  code="${2}"

  print_coloured "ERROR: ${text}" "red" >&2 &&
  exit "${code}"
) }

error_arguments_contained () { (
  error "Option \"--${OPTARG}\" should not contain any arguments.\n" "2" >&2
) }

error_option_invalid () { (
  error "Option \"--${OPTARG}\" is invalid.\n" "2" >&2
) }

# trap function
# todo: test if directory of file in "${line}" is empty, if so, delete.
notify_exit_early () {
  list_rm="${1}"
# check that list_rm has been set, is not null and points to an actual file
  if test "${list_rm:+set}" = "set" && test "${list_rm}" != "" && test -f "$(readlink --canonicalize "${list_rm}")" ; then
    while IFS='
' read -r line ; do
      if test -e "${line}" ; then
#       ${line} is a valid file, delete it
        if test -w "${line}" ; then
          rm --recursive "${line}"
        else
          sudo rm --recursive "${line}"
        fi
      fi
    done < "${list_rm}" &&
    rm "${list_rm}"
  fi &&
  error 'Exited too early.\n' "2" >&2
}

warning () { (
  text="${1}"

  print_coloured "WARNING: ${text}\n" "yellow" >&2
) }

warning_privilege () { (
  group="$(id -g)"
  if test "${group}" -eq 0 || test "${group}" -eq 4 ; then
    warning 'Running as privileged user.\nContinue? [N/y]: ' >&2 &&
    while : ; do
      read -r input &&
      input="$(printf '%s' "${input}" | tr '[:upper:]' '[:lower:]')"
      case "${input}" in
        "y" | "yes" )
          break
        ;;
        "" | "n" | "no" )
#         argument passed to `exit` must != null or 0 to exit sub shell. not sure why though. could be due to `set -e`.
          exit 1
        ;;
        * )
#         argument passed to `exit` must != null or 0 to exit sub shell. not sure why though. could be due to `set -e`.
          warning "Unrecognised input \"${input}\", falling back to default option \"N\"." >&2 &&
          exit 1
        ;;
      esac
    done
  fi
) }
